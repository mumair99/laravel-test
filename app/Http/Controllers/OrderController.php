<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Order;
use App\Models\PaymentGateway;

class OrderController extends Controller
{

    /**
     * Create new orders
     */
    public function placeOrder(Request $request)
    {
        $request->validate([
            'order_number' => 'required|max:100',
            'price' => 'required'          
        ]);
        
        $order = new Order;
        $order->order_number = $request->order_number;
        $order->price = $request->price;
        $order->payment_gateway_id = PaymentGateway::getPaymentGatewayId();  // Get payment gateway id      

        try{
            $order->save();
            return response()->json(['success' => 'Created Successfully']);
        }
        catch(\Exception $ex){
            return response()->json(['error' => $ex->getMessage()]);
        }
    }
}
