<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PaymentGateway;

class PaymentGatewayController extends Controller
{
    /**
     * Create new payment gateways
     */
    public function createPaymentGateway(Request $request)
    {
        $request->validate([
            'name' => 'required|max:100',
            'ratio' => 'required'          
        ]);

        $paymentGateway = new PaymentGateway;
        $paymentGateway->name = $request->name;
        $paymentGateway->ratio = $request->ratio;

        try{
            $paymentGateway->save();
            return response()->json(['success' => 'Created Successfully']);
        }
        catch(\Exception $ex){
            return response()->json(['error' => $ex->getMessage()]);
        }
        
    }

    /**
     * Update existing payment gateways
     */
    public function updatePaymentGateway(Request $request)
    {
        $request->validate([
            'name' => 'required|max:100',
            'ratio' => 'required'          
        ]);

        $paymentGateway = PaymentGateway::find($request->id);
        $paymentGateway->name = $request->name;
        $paymentGateway->ratio = $request->ratio;
        
        try{
            $paymentGateway->save();
            return response()->json(['success' => 'Updated Successfully']);
        }
        catch(\Exception $ex){
            return response()->json(['error' => $ex->getMessage()]);
        }
    }
}
