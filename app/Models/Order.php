<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\PaymentGateway;

class Order extends Model
{
    use HasFactory;

    /**
     * Get the payment gateway associated with the order.
     */
    public function paymentGateway()
    {
        return $this->hasOne(PaymentGateway::class);
    }
}
