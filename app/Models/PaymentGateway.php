<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Order;

class PaymentGateway extends Model
{
    use HasFactory;

    /**
     * Get the order that owns the payment gateway.
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * Get  payment gateway id for new order.
     */
    public static function getPaymentGatewayId()
    {
        $payment_gateway_id = 0;

        $paymentGatewaySum = self::sum('ratio');  
        $paymentGatewayCount = self::count();
        $paymentGateways = self::get()->toArray();
        $orderCount = Order::count();

        if ($orderCount < $paymentGatewaySum) {
            $orderInfos = DB::table('orders')
            ->select('payment_gateway_id', DB::raw('count(*) as payment_count'))
            ->groupBy('payment_gateway_id')
            ->get();
            
            if (count($orderInfos) <= 0) {
                $payment_gateway_id = self::first()->id;
            }
            else{
                foreach ($paymentGateways as $key => $paymentGateway) {
                    $order = $orderInfos->where('payment_gateway_id',$paymentGateway['id']);
                    if (empty($order[0]) || $order[0]->payment_count < $paymentGateway['ratio']) {
                        $payment_gateway_id = $paymentGateway['id'];
                        break;
                    }
                    else if ($key == array_key_last($paymentGateways)) {
                        $payment_gateway_id = self::first()->id;
                    }
                    
                }
            }
        } else {
            $orderOffset = (floor($orderCount / $paymentGatewaySum))*$paymentGatewaySum;
            $orderIds = DB::table('orders')->skip($orderOffset)->take($paymentGatewaySum)->pluck('id')->toArray();

            $orderInfos = DB::table('orders')->whereIn('id',$orderIds)
            ->select('payment_gateway_id', DB::raw('count(*) as payment_count'))
            ->groupBy('payment_gateway_id')
            ->get();

            if (count($orderInfos) <= 0) {
                $payment_gateway_id = self::first()->id;
            }
            else{
                foreach ($paymentGateways as $key => $paymentGateway) {
                    $order = $orderInfos->where('payment_gateway_id',$paymentGateway['id']);
                    if (empty($order[0]) || $order[0]->payment_count < $paymentGateway['ratio']) {
                        $payment_gateway_id = $paymentGateway['id'];
                        break;
                    } else if ($key == array_key_last($paymentGateways)) {
                        $payment_gateway_id = self::first()->id;
                    }
                    
                }
            }
        }

        return $payment_gateway_id;
    }
}
