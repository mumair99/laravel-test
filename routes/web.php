<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaymentGatewayController;
use App\Http\Controllers\OrderController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/create_payment_gateway', [PaymentGatewayController::class, 'createPaymentGateway'])->name('create-payment-gateway');
Route::post('/update_payment_gateway', [PaymentGatewayController::class, 'updatePaymentGateway'])->name('update-payment-gateway');

Route::post('/place_order', [OrderController::class, 'placeOrder'])->name('place-order');